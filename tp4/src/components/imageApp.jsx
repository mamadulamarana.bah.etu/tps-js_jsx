import React from 'react';

import '../assets/style/murImages.css';
import ImageWall from './ImageWall.jsx';
import ImageDetails from './ImageDetails.jsx';

import dataImages from '../data/dataImages.js';

/*
 define root component
*/
export default class ImageApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {image : "../images/image5.jpg",
                  texte : "la plus belle....",
                  filterText : ''
                };
  }

  imageChanged(newImage, newTexte) {
    this.setState( {image : newImage, texte : newTexte} );
  }
  filterChanged(newFilterText) {
    this.setState( {filterText : newFilterText} );
  }

  render() {
    const imageChanged = this.imageChanged.bind(this);
    const filter = this.filterChanged.bind(this);
    return (
      <div> <ImageWall images={dataImages}
                       imageChanged={imageChanged}
                       filterText={this.state.filterText}
                       />
            <ImageDetails {...this.state} 
                          filterChanged={filter}
                          filterText={this.state.filterText}
                          />
      </div>
    );
  }
  
}
