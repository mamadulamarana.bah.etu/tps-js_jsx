import React from 'react';

export default class ImageWall extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const images = this.props.images.filter(image => image.texte.includes(this.props.filterText.toLowerCase()) )
                                        .map(
                                            image => <img src={image.image} 
                                            key={image.image} 
                                            alt={image.texte} 
                                            title={image.texte}
                                            onMouseOver = { () => this.props.imageChanged(image.image, image.texte) }
                                            />);
        return (<div id = "mur"> {images} </div>);
    }

}