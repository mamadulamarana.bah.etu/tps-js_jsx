import Animation from "./animation";
import Ball from './ball';
import KeyManager from './keyManager';


export default class AnimationWithObstacle extends Animation {

    constructor(canvas, obstacle) {
        super(canvas);
        this.obstacle = obstacle;
        this.KeyManager = new KeyManager();
    }

    animate() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        // this.obstacle.draw(this.context);

        this.obstacle.handleMoveKeys(this.KeyManager);
        this.obstacle.move(this.canvas);
        this.obstacle.draw(this.context);

        this.balls.forEach(ball => ball.move(this.canvas)); 
        this.balls = this.balls.filter(ball => !ball.collisionWith(this.obstacle));
        this.balls.forEach(ball=> ball.draw(this.context));
        this.start = true;
        this.animationframe = window.requestAnimationFrame( this.animate.bind(this) );    
    }

    keyDownActionHandler(event) {
        switch (event.key) {
            case "ArrowLeft":
            case "Left":
                this.KeyManager.leftPressed();
                break;
            case "ArrowRight":
            case "Right":
                this.KeyManager.rightPressed();
                break;
            case "ArrowUp":
            case "Up":
                this.KeyManager.upPressed();
                break;
            case "ArrowDown":
            case "Down":
                this.KeyManager.downPressed();
                break;
            default: return;
        }
        event.preventDefault();
    }

    keyUpActionHandler(event) {
        switch (event.key) {
            case "ArrowLeft":
            case "Left":
                this.KeyManager.leftReleased();
                break;
            case "ArrowRight":
            case "Right":
                this.KeyManager.rightReleased();
                break;
            case "ArrowUp":
            case "Up":
                this.KeyManager.upReleased();
                break;
            case "ArrowDown":
            case "Down":
                this.KeyManager.downReleased();
                break;
            default: return;
        }
        event.preventDefault();
        }

}