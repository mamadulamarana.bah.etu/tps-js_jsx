import Ball from './ball';

/* TYPE Animation */
export default class Animation {

  constructor(canvas) {
    this.canvas = canvas;
    this.context = this.canvas.getContext('2d');
    this.animationframe = null;
    this.balls = new Array();
    this.start = false;
  }

  /* start the animation or stop it if previously running */
  startAndStop() {
    if (this.start) {
      window.cancelAnimationFrame(this.animationframe);
      this.start = false;
    }
    else {
      this.animate();
      this.start = true;
    }
  }

  addBall() {
    const x = this.#alea(this.canvas.width - Ball.BALL_WIDTH);
    const y = this.#alea(this.canvas.height - Ball.BALL_WIDTH);
    const ball = new Ball(x, y, this.#alea(-5), this.#alea(5));
    this.balls.push(ball);
  }

  #alea(n) {
    return Math.floor(Math.random()*(n+1));
  }

  animate() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.balls.forEach(ball => ball.move(this.canvas));
    this.balls.forEach(ball=> ball.draw(this.context));
    this.start = true;
    this.animationframe = window.requestAnimationFrame( this.animate.bind(this) );

    //window.addEventListener("click", ()=>window.cancelAnimationFrame(this.animationframe);
    //this.animationframe = window.requestAnimationFrame( () => this.animate() );
  }
}
