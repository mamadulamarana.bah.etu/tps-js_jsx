
// la source de l'image à utiliser pour la balle
import ballImgSrc from './assets/images/ball.png';
import Obstacle from './Obstacle';

/* TYPE Ball */
export default class Ball {

	static BALL_WIDTH = 48;

  constructor(x, y, deltaX = 3, deltaY = -2) {
    // A COMPLETER...
    this.image = this.#createImage(ballImgSrc);
    this.x = x;
    this.y = y;
    this.deltaX = deltaX;
    this.deltaY = deltaY;
  }

  /* draw this ball, using the given drawing 2d context */
  draw(context) {
    context.drawImage(this.image, this.x, this.y);
  }

  move(canvas) {
    let tmp1 = (this.x + this.deltaX);
    let tmp2 = (this.y + this.deltaY);

    if ( tmp1 + this.width > canvas.width) {
      this.deltaX = - this.deltaX;
    }
    if (tmp2 + this.height > canvas.height) {
      this.deltaY = -this.deltaY;
    }
    if ( (this.x + this.deltaX) < 0) {
      this.deltaX = -this.deltaX;
    }
    if ( (this.y + this.deltaY) < 0) {
      this.deltaY = -this.deltaY;
    }
    this.x += this.deltaX;
    this.y += this.deltaY;

  }


  /* crée l'objet Image à utiliser pour dessiner cette balle */
  #createImage(imageSource) {
	  const newImg = new Image();
  	newImg.src = imageSource;
  	return newImg;
  }
  get width() {
    return this.image.width;
  }
  get height() {
    return this.image.height;
  }

  collisionWith(obstacle) {
    const a1x = obstacle.x;
    const a1y = obstacle.y;
    const a2x = this.x + Ball.BALL_WIDTH;
    const a2y = this.y + Ball.BALL_WIDTH;

    const a2P_x = obstacle.x + obstacle.width;
    const a2P_y = obstacle.y + obstacle.height;

    const p1_x = Math.max(this.x, a1x);
    const p1_y = Math.max(this.y, a1y);
    const p2_x = Math.min(a2x, a2P_x);
    const p2_y = Math.min(a2y, a2P_y);

    if ( (p1_x < p2_x) & (p1_y < p2_y) ) {
      return true;
    }
    else {
      return false;
    }
  }

}
