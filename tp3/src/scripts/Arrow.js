import GameElement from "./GameElement";
import image from "./assets/images/fleche.png";

export default class Arrow extends GameElement {

    constructor(x, y) {
        super(x, y, 0, -8, image);
    }

}