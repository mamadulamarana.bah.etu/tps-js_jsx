import GameElement from "./GameElement";
import image from "./assets/images/cible.png";

export default class Cible extends GameElement{
    constructor(x, y) {
        super(x, y, 0, 0, image);
    }
        
}