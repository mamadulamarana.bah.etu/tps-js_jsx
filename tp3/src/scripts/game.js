import Arc from "./Arc.js"; 
import KeyManager from "./keyManager.js";
import Cible from "./Cible.js";
import Arrow from "./Arrow.js";
import Carquois from "./Carquois.js"
import Oiseau from "./Oiseau.js";
import image1 from "./assets/images/oiseau-voleur-droite-gauche.png";
import image2 from "./assets/images/oiseau-voleur-gauche-droite.png";

export default class Game {

   #canvas;
   #context;
   #animationframe;
   #arc;
   #cible;
   #arrows;
   #carquois;
   #oiseaux;
   #nb_arrows;
   #start;
   #keyManager;
   #score;
   #vies;
   #vie;
   tmp;

   constructor(canvas) {
      this.#canvas = canvas;
      this.#context = this.canvas.getContext('2d');
      this.#animationframe = null;
      this.#arc = this.#createArc();
      this.#cible = this.#createCible();
      this.#carquois = new Carquois( this.#alea(this.#canvas.width) ,this.#alea(300) + 100 );
      this.#oiseaux = [];
      this.#arrows = [];
      this.#nb_arrows = 5;
      document.getElementById("nbArrows").textContent = this.#nb_arrows;
      this.#score = 0;
      this.#vie = 3;
      this.#vies = document.getElementById("lifes");
      this.#keyManager = new KeyManager();
      this.#start = false;
      this.tmp = null;
   }

   /** donne accès au canvas correspondant à la zone de jeu */
   get canvas() {
      return this.#canvas;
   }
   get context() {
    return this.#context;
   }
   get vies() {
    return this.#vie;
   }
   get score() {
    return this.#score;
   }
   
   #createArc() {
    return new Arc(this.#canvas.height/2- 85, this.#canvas.width + 85);
   }
   #createCible() {
    return new Cible(Math.max(0, Math.min(this.#alea(this.#canvas.width), this.#canvas.width - 64)), this.#canvas.height/(this.#canvas.height/2));
   }
   #createArrow() {
    return new Arrow(this.#arc.x + 85/2, this.#arc.y - 85);
   }
   #createCarquois() {
    if (Math.random() > 0.5) {
        this.#carquois = new Carquois( this.#alea(this.#canvas.width), (this.#alea(300) + 100) );
    }
   }
   #createOiseau() {
    if (Math.random() < 0.75) {
        if ( Math.random() < 0.5) {
            this.#oiseaux.push(new Oiseau( this.#canvas.width, (this.#alea(300) + 100), -4, image1));
        }
        else {
            this.#oiseaux.push(new Oiseau(0, this.#alea(300) + 100, 4, image2));
        }
    }
   }


   #alea(n) {
    return Math.floor(Math.random()*(n));
  }

  
  #shoot() {
    if (this.#nb_arrows > 0) {
        const arrow = this.#createArrow();
        this.#arrows.push(arrow);
        this.#nb_arrows--;
        document.getElementById("nbArrows").textContent = this.#nb_arrows;
    }
  }

  #collisionCible() {
    this.#arrows.forEach(arrow => {
        if (arrow.collisionWith(this.#cible)) {
            this.#arrows = this.#arrows.filter(arrow => !arrow.collisionWith(this.#cible));
            this.#score += 1000;
            document.getElementById("score").textContent = this.#score;
            this.#cible = this.#createCible();
        }
    })
  }

  #collisionCarquois() {
    if (this.#arc.collisionWith(this.#carquois)) {
        this.#nb_arrows = 5;
        document.getElementById("nbArrows").textContent = this.#nb_arrows;
        this.#carquois = new Carquois( this.#alea(this.#canvas.height), (this.#alea(300) + 100) );
    }
    this.#oiseaux.forEach(oiseau => {
        if (oiseau.collisionWith(this.#carquois)) {
          console.log(this.#carquois.width);
            this.#carquois = new Carquois( this.#alea(this.#canvas.height), (this.#alea(300) + 100) );
        }
    }) 
  }

  #collisionOiseauWithArrow() {
    let tmpOiseau;
    this.#arrows.forEach(arrow => {
        tmpOiseau = this.#oiseaux.filter(oiseau => !oiseau.collisionWith(arrow));
    });
    return tmpOiseau;
  }

  #collisionArrowWithOiseau() {
    let tmpArrow;
    this.#oiseaux.forEach(oiseau => {
        tmpArrow = this.#arrows.filter(arrow => !arrow.collisionWith(oiseau));
    })
    return tmpArrow;
  }

  #collisionOiseauWithArc() {
    this.#oiseaux.forEach(oiseau => {
        if (oiseau.collisionWith(this.#arc)) {
            this.#oiseaux = this.#oiseaux.filter(oiseau => !oiseau.collisionWith(this.#arc));
            const removedLife = document.getElementById("life-"+ this.#vie);
            this.#vies.removeChild(removedLife);
            this.#vie--;
        }
    })
  }


   startAndStop() {
    let interval1= 0;
    let interval2 = 0;
      if (this.#start) {
        clearInterval(interval1);
        clearInterval(interval2);
        window.cancelAnimationFrame(this.#animationframe);
        this.#start = false;
        document.getElementById("stopAndStartGame").textContent = "Jouer"
      }
      else {
        this.animate();
        this.#start = true;
        document.getElementById("stopAndStartGame").textContent = "Stop";
        interval1 = setInterval(this.#createCarquois.bind(this), 1500);
        interval2 = setInterval(this.#createOiseau.bind(this), 1000);
      }
    }


    animate() {
      if (this.#vie == 0) {
        window.cancelAnimationFrame(this.#animationframe);
        alert("vous n'avez plus d'arc vous avez perddu"); 
      }

      this.#context.clearRect(0, 0, this.#canvas.width, this.#canvas.height);

      this.#arc.handleMoveKeys(this.#keyManager);
      this.#arc.move(this.#canvas);
      this.#arc.draw(this.#context);
      this.#cible.draw(this.#context);

      this.#arrows.forEach(arrow => arrow.move(this.#canvas));
      this.#arrows.forEach(arrow => arrow.draw(this.#context));
      this.#arrows = this.#arrows.filter(arrow => arrow.y > 0);
      this.#collisionCible();

      this.#carquois.draw(this.#context);
      this.#oiseaux.forEach(oiseau => {
        oiseau.move(this.#canvas);
        oiseau.draw(this.#context);
      });

      this.#collisionCarquois();
      this.#oiseaux = this.#oiseaux.filter(oiseau => oiseau.x + oiseau.width > 0 && oiseau.x <= this.#canvas.width);
      const tmpOiseaux = this.#collisionOiseauWithArrow();
      const tmpArrows = this.#collisionArrowWithOiseau();
      this.#collisionOiseauWithArc();
      if (tmpOiseaux && tmpArrows) {
        this.#oiseaux = tmpOiseaux;
        this.#arrows = tmpArrows;
      }

      this.#animationframe = window.requestAnimationFrame( this.animate.bind(this) );
  
      //window.addEventListener("click", ()=>window.cancelAnimationFrame(this.animationframe);
      //this.animationframe = window.requestAnimationFrame( () => this.animate() );
    } 

   keyDownActionHandler(event) {
      switch (event.key) {
          case "ArrowLeft":
          case "Left":
              this.#keyManager.leftPressed();
              break;
          case "ArrowRight":
          case "Right":
              this.#keyManager.rightPressed();
              break;
          case "ArrowUp":
          case "Up":
              this.#keyManager.upPressed();
              break;
          case "ArrowDown":
          case "Down":
              this.#keyManager.downPressed();
              break;
          case "Space":
          case " ":
              this.#shoot();
          default: return;
      }
      event.preventDefault();
  }

  keyUpActionHandler(event) {
      switch (event.key) {
          case "ArrowLeft":
          case "Left":
              this.#keyManager.leftReleased();
              break;
          case "ArrowRight":
          case "Right":
              this.#keyManager.rightReleased();
              break;
          case "ArrowUp":
          case "Up":
              this.#keyManager.upReleased();
              break;
          case "ArrowDown":
          case "Down":
              this.#keyManager.downReleased();
              break;
          default: return;
      }
      event.preventDefault();
      }


}



