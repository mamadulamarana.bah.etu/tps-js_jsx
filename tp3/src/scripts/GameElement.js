
export default class GameElement {

    constructor(x, y, deltaX = 0, deltaY = 0, imgSrc) {
        this.image = this.#createImage(imgSrc);
        this.x = x;
        this.y = y;
        this.deltaX = deltaX;
        this.deltaY = deltaY;
    }

    draw(context) {
        context.drawImage(this.image, this.x, this.y);
      }
    
    move(canvas) {
        this.x += this.deltaX;
        this.y += this.deltaY;
      }

    #createImage(imageSource) {
        const newImg = new Image();
        newImg.src = imageSource;
        return newImg;
    }

    get width() {
      return this.image.width;
    }
    get height() {
      return this.image.height;
    }

    collisionWith(obstacle) {
      const a1x = obstacle.x;
      const a1y = obstacle.y;
      const a2x = this.x + this.width;
      const a2y = this.y + this.height;
  
      const a2P_x = obstacle.x + obstacle.width;
      const a2P_y = obstacle.y + obstacle.height;
  
      const p1_x = Math.max(this.x, a1x);
      const p1_y = Math.max(this.y, a1y);
      const p2_x = Math.min(a2x, a2P_x);
      const p2_y = Math.min(a2y, a2P_y);
      if ( (p1_x < p2_x) && (p1_y < p2_y) ) {
          return true;
        }
        else {
          return false;
        }      
   }

  
    
}