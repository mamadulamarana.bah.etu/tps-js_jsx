import GameElement from "./GameElement";
import image from "./assets/images/fleches.png";

export default class Carquois extends GameElement {

    constructor(x, y) {
        super(x, y, 0, 0, image);
    }
}