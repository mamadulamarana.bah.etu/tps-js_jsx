import GameElement from "./GameElement";
import KeyManager from "./keyManager.js";
import image from "./assets/images/arc.png";

export default class Arc extends GameElement{

    static NB_ARROW = 5;
    #nb_arrow;
    #broke;

    constructor(x, y) {
        super(x, y, 0, 0, image);
        this.#nb_arrow = GameElement.NB_ARROW;
    }
    get nb_arrow() {
        return this.#nb_arrow;
    }

    brokeArc() {
        this.#broke = true;
    }

    moveLeft() {              
        this.deltaX = this.deltaX - 10;   // le déplacement se fera vers la gauche, par pas de 10px
     }
     moveRight() {
        this.deltaX = this.deltaX + 10;   // le déplacement se fera vers la droite, par pas de 10px
     }
     moveUp() {
      this.deltaY = this.deltaY -10;
     }
     moveDown() {
      this.deltaY = this.deltaY +10;
     }
     stopMoving() {
        this.deltaX = 0;
        this.deltaY = 0;
     }

     move(box) { 
        this.x = Math.max(0, Math.min(box.width - this.width, this.x + this.deltaX));
        this.y = Math.max(100, Math.min(box.height - this.height, this.y + this.deltaY));
     }

     handleMoveKeys(KeyManager) {
        this.stopMoving();    // on réinitialise les déplacements
        if (KeyManager.left)  // touche flèche gauche pressée ?
           this.moveLeft();
        if (KeyManager.right) // touche flèche droite pressée ?
           this.moveRight();
        if (KeyManager.up)
           this.moveUp();
        if (KeyManager.down)
           this.moveDown();
     }
  

     shoot() {
        if (this.#nb_arrow > 0) {
            this.#nb_arrow--;
        }
     }

    
    // collisionWithOiseau(oiseau){
    //     if (this.collisionWith(oiseau)) {
    //         this.brokeArc();
    //     }
    // }

    // collisionWithCarquois(carquois) {
        
    // }
     
     
}