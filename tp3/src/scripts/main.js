
// importation de la classe Game.js
import Game from './game.js';


const init = () => {
   const canvas = document.getElementById("playfield");
   const game = new Game(canvas);
   document.getElementById("stopAndStartGame").addEventListener("click", event => { 
                                                                                    game.startAndStop() ;
                                                                                    event.target.blur();
                                                                                  });
   window.addEventListener('keydown', game.keyDownActionHandler.bind(game));
   window.addEventListener('keyup', game.keyUpActionHandler.bind(game)); 
}

window.addEventListener("load", init);

//
console.log('le bundle a été généré');
