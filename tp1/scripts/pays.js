const numberFormatter = new Intl.NumberFormat();
/**** Question 1 *****/
console.log(' *** QUESTION 1 ***');

dataPays.forEach( pays => pays.toString = function() { return `${this.nom} : ${this.population} hab., ${this.superficie}km², ${this.PIB} milliardsUS$` } );
console.log(dataPays.slice(0, 5).toString());

/****  QUESTION 2 *****/
console.log(' *** QUESTION 2 ***');

const populationTotale = dataPays.reduce((previous, element) => previous + element.population, 0);
console.log(`population totale : ${numberFormatter.format(populationTotale)}`);
/***********************/


/****  QUESTION 3 *****/
console.log(' *** QUESTION 3 ***');
const findData = (nom) => dataPays.find( pays => pays.nom === nom);

console.log(findData('France').toString());

/***********************/


/****  QUESTION 4 *****/
console.log(' *** QUESTION 4 ***');
const dixpluspeuples = (dataPays.sort( (a, b) => b.population - a.population)
                                .slice(0, 10)
                                .map(pays => pays.nom));

console.log(`10 plus peuplés : ${dixpluspeuples}`);
/***********************/

/****  QUESTION 5 *****/
console.log(' *** QUESTION 5 ***');
const densite = (pays) => ({
    nomPays : pays.nom,
    densite : (pays.population / pays.superficie).toFixed(2),
    toString : function() {return `${this.nomPays} : ${this.densite} hab/km²`}
})
let res;
res = dataPays.map(pays => densite(pays)).filter(pays => pays.densite > 1000)
              .sort( (a,b) => b.densite - a.densite)
              .forEach(pays => console.log(pays.toString()));
/***********************/


/****  QUESTION 6 *****/
console.log(' *** QUESTION 6 ***');

let nb_pop = dataPays.map(pays => ({...pays, PIB_hab : (pays.PIB*1000000) / pays.population}) )
        .filter(pays => pays.PIB_hab < 10000)
        .reduce( (previous, element) => previous + element.population, 0);
console.log(numberFormatter.format(nb_pop));
/***********************/
