'use strict';

// une liste pour des tests
const numbers = [2, 3, 5, 4, 10, 6];

/********** EXERCICE 0 ***********************/
console.log(` *** EXERCICE 0 *** `);
/* computes the double of its parameter
 * @param x (number) a number
 * @return (number) the double of *x*
*/
const example = x => x * 2;
// tests
console.log(`example(10) : ${example(10)}`);
console.log(`example(21) : ${example(21)}`);



/* filter and keep the elements of *list* smaller than *max*
 * @param list (Array) list of elements
 * @param max (Any) upper bound filter value
 * @return (Array) list of elements of *list* smaller than *max*
*/
const example2 = (list, max) => list.filter( elt => elt < max );
// tests
console.log(`example2(numbers, 5) : ${example2(numbers, 5)}`);

/*********************************************/

/********** EXERCICE 1 ***********************/
console.log(` *** EXERCICE 1 *** `);
const persons = [ {name : 'timoleon', age : 12 }, {name : 'bilbo', age : 111 }, {name : 'frodo', age : 33 }];

console.log("Q1 :");
numbers.forEach(number => console.log(`${number}`));

console.log("Q2 :");
const expr = persons[1];
console.log(expr.name);

console.log("Q3 :");
persons.forEach(person => console.log(`${person.name} a ${person.age} ans`));

console.log("Q4 :");
const myForEach = (tab, fct) => {
    for (let i in tab) {
        fct(tab[i], i, tab);
    }
}

console.log("Q5 :");
myForEach(numbers, number => console.log(`${number}`));
myForEach(persons, person => console.log(`${person.name} a ${person.age} ans`));

/*********************************************/


/********** EXERCICE 2 ***********************/
console.log(` *** EXERCICE 2 *** `);
console.log("Q1 :");
numbers.map(number => console.log(number * number));

console.log("Q2 :");
const multiples = (n, l) => l.map(elmnt => elmnt * n);
console.log(multiples(10, numbers));

console.log("Q3 :");
const multiples5 = (l) => multiples(5, l);
console.log(multiples5(numbers));

console.log("Q4 :");
const multiplesFactory = (factor) => {
    return  l => {multiples(factor, l)};
}
const multiples100 = multiplesFactory(100);
console.log(typeof(multiples100));
multiples100(numbers);

/*********************************************/


/********** EXERCICE 3 ***********************/
console.log(` *** EXERCICE 3 *** `);
console.log("Q1 :");
const capitalize = (str) => `${str.substring(0,1).toUpperCase() + str.substring(1)}`;
console.log(capitalize('timoleon'));

console.log("Q2 :");
let capitalize_obj = persons.map(person => (capitalize(person.name))) ;
console.log(capitalize_obj);

/**Q3 */
const myMap = (tab, fct) => {
    let res = [];
    for (let i in tab) {
        res.push(fct(tab[i], i, tab));
    }
    return res;
}

console.log("Q4 :");
console.log(myMap(persons, person => (capitalize(person.name))) );

console.log("Q5 Facultatif :");

console.log("Q6 Facultatif :");

/*********************************************/


/********** EXERCICE 4 ***********************/
console.log(` *** EXERCICE 4 *** `);
console.log("Q1 :");
let filtre = numbers.filter(number => number < 5);
console.log(filtre);

console.log("Q2 :");
const createAcronym = (phrase) => {
    return phrase.split(' ')
                 .filter(mot => mot.length > 3)
                 .map(mot => mot.substring(0, 1).toUpperCase())
                 .join('');
}
console.log(createAcronym('formations en informatique de lille'));
console.log(createAcronym('société nationale des chemins de fer français'));

/*********************************************/

/********** EXERCICE 5 ***********************/
console.log(` *** EXERCICE 5 *** `);
console.log("Q1 :");
const nbLetters = (phrase) => {
    return phrase.split(' ')
                 .reduce( (previous, elmnt) => previous + elmnt.length, 0);
}
const FIL = "Formations en Informatique de Lille";
console.log(nbLetters(FIL));

console.log("Q2 :");
const max = (n1, n2) => (n1>n2 ? n1: n2);

const maxNumber = (l) => l.reduce((previous, elmnt) => max(previous, elmnt), 0);
console.log(maxNumber(numbers));

console.log("Q3 :");
const maxNumber2 = (l) => Math.max(...l);
console.log(maxNumber2(numbers));

console.log("Q4 :");
const sum = (...number) => (number.length == 0 ? 0 :
    number.reduce((previous, element) => previous + element, 0)); 
console.log(sum());
console.log(sum(1,2));
console.log(sum(1,2,3,4,5))
console.log(sum(...numbers));

console.log("Q5 Facultatif:");

/*********************************************/

/********** EXERCICE 6 ***********************/
console.log(` *** EXERCICE 6 *** `);

const lesInvites = ['Tim Oleon', 'Timo Leon', 'Bilbo', 'Frodo', 'Sam', 'Merry', 'Pippin']
const lesReponses = [
                  {nom : 'Sam', present : 'oui'},
                  {nom : 'Tim Oleon', present : 'non'},
                  {nom : 'Bilbo', present : 'oui'},
                  {nom : 'Frodo', present : 'oui'},
                  {nom : 'Timo Leon', present : 'non'},
                 ];

console.log("Q1 :");
const participants = (invites, reponses) => {
    let res = [];
    res.push( ...(reponses.filter(obj => obj.present === 'oui')
                  .map(obj => obj.nom)) );
    return res;
} 
console.log(participants(lesInvites, lesReponses))

/*********************************************/

/********** EXERCICE 7 ***********************/
console.log(` *** EXERCICE 7 *** `);

/*********************************************/

/********** EXERCICE 8 ***********************/
console.log(` *** EXERCICE 8 *** `);



/*********************************************/
