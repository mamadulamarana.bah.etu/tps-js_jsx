import ProductZone from "./ProductZone.jsx";

export default class Boutique extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const filtreInput = <input
        id="filtre" type="text" placeholder="filtrer les produits..."
        value = {this.props.filterText}
        onChange = { event => this.props.filterChanged (event.target.value) }
        />;

        return <div className="productList">
            <h4> Boutique </h4>
            <div className="productsZone">
                < ProductZone products = {this.props.products}
                              addToCart = {this.props.addToCart}
                              filterText = {this.props.filterText}
                />
            </div>
            <div className="filter" > {filtreInput} </div>
        </div>
    }
}