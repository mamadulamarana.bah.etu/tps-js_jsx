import '../assets/style/product.css';
import '../assets/style/productList.css';
import imgPoubelle from '../assets/images/poubelle.jpg';

/**
 * Produit dans le panier
 */
export default class ProductCart extends React.Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.state = {inputValue : 1};
    }

    /**
     * supprime un produit du panier
     * declenché par l'venement click du button poubelle
     */
    handleClick() {
        this.props.deleteToCart(this.props.product.id, this.state.inputValue);
    }

    /**
     * declenché par l'venement onchange du input
     * @param {*} value valeur du input (en string)
     */
    handleInputChange(value) {
        this.setState( (prevState) => (
            {inputValue : ((parseInt(value) > prevState.inputValue)?
                this.props.decreaseStock(this.props.product.id, value) : this.props.increaseStock(this.props.product.id, value))
            }
            
        ) )
    }

    render() {
        const product = this.props.product;
        const image = <img src={product.image}                          
                           alt={product.description}
                           title={product.description}
                    />

        const cartPoubelle = <img className='button'
                            src={imgPoubelle}
                            alt='imagePanier'
                            title='imagePanier'
                            onClick={this.handleClick}
                        />

        return  <div className='product' id={product.id}>
                    <div className='info'>
                        <div className='name'> {product.name} </div>
                    </div>
                    <div className='imageProduit'> {image} </div>
                    <input type="number" name ="qte" id="qte"
                           min={1}
                           defaultValue={1}
                           max={this.props.product.stock + this.state.inputValue}
                           onChange = { event => this.handleInputChange(event.target.value) }
                    />
                    {cartPoubelle}
                </div>
    }
}