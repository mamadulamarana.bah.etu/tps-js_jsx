
import Product from './Product.jsx';
import '../assets/style/productList.css';


export default class ProductZone extends React.Component {

    constructor(props) {
        super(props);
    }

    buildProduct() {
        const products = this.props.products;
        const FilterProducts = products.filter(product => product.name.toLowerCase().includes(this.props.filterText.toLowerCase()))
                           .map(product => <Product product = {product}
                                                    addToCart={this.props.addToCart}
                                                    key={product.id}
                                            />
                            )
        return FilterProducts;
    }

    render() {
        return <div className="productsZone">
            { this.buildProduct() }
        </div>
    }
}




