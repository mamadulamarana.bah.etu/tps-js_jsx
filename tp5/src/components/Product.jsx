import '../assets/style/product.css';
import '../assets/style/productList.css';
import imgCart from '../assets/images/panier.jpg';

export default class Product extends React.Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.addToCart(this.props.product.id);
    }

    render() {
        const product = this.props.product;
        const image = <img src={product.image}                          
                           alt={product.description}
                           title={product.description}
                    />

        const cartImg = <img className='button'
                            src={imgCart}
                            alt='imagePanier'
                            title='imagePanier'
                            onClick={this.handleClick}
                        />

        return  <div className='product' id={product.id}>
                    <div className='info'>
                        <div className='name'> {product.name} </div>
                        <div className='weight'> {product.weight} </div>
                        <div className='description'> {product.description} </div>
                    </div>
                    <div className='imageProduit'> {image} </div>
                    <div className='price'> {product.price} </div>
                    <div className='stock'> qté {product.stock} </div>
                    {cartImg}
                </div>
    }
}