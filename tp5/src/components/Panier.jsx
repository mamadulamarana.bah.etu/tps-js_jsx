import Product from './Product.jsx';
import ProductCart from './ProductCart.jsx'
import '../assets/style/cart.css';
import imgPoubelle from '../assets/images/poubelle.jpg';
import produits from '../data/products.js';


export default class Panier extends React.Component {

    constructor(props) {
        super(props);
    }

    buildProduct() {
        const products = this.props.products.map(product => <ProductCart product= { product }
                                                                     key= { product.id }
                                                                     increaseStock={this.props.increaseStock}
                                                                     decreaseStock={this.props.decreaseStock}
                                                                     deleteToCart={this.props.deleteToCart}
                                                            />);
        return products;
    }

    render() {

        return <div className="cart">
            <h4> Panier </h4>
            <div className='weight'> poids : {this.props.weight} </div>
            {this.buildProduct()}
            <div className="total"> total commande : {this.props.price}</div>
        </div>
    }
}