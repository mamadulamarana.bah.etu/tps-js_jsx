import React from 'react';
import Boutique from './Boutique.jsx';
import Panier from './Panier.jsx';
import produits from '../data/products.js';
import '../assets/style/app.css';
import '../assets/style/cart.css';
import products from '../data/products.js';

/*
 define root component
*/
export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = { products : [], cartProduct : [],
                   filterText : '', totalPrice : 0, 
                   weight : 0
                  };
  }

  initProducts() {
    return [...products];
  }

  componentDidMount() {
    setTimeout(  () => this.setState( { products : this.initProducts() }), 10);
  }

  /**
   * ajoute un produit dans le panier
   * @param {*} productId id du produit
   */
  addToCart(productId) {
    const prod = this.state.products.find( product => product.id == productId)
    if (!this.state.cartProduct.find(product => (product.id == productId) )) {
      if (prod.stock > 0) {
        prod.stock = prod.stock - 1;
        const newCart = [...this.state.cartProduct, prod];
        const newPrice = this.state.totalPrice + prod.price; /** gestion du prix */
        const newWeight = this.state.weight + prod.weight; /**gestion du poids */
        this.setState( {cartProduct : newCart, totalPrice : newPrice, weight : newWeight} );  
      }  
    }
  }

  /**
   * supprime un produit du panier
   * @param {*} productId id du produit
   * @param {*} inputValue value du input dans le panier
   */
  deleteToCart(productId, inputValue) {
    const product = this.state.cartProduct.find( product => product.id == productId);
    const stock = product.stock + parseInt(inputValue);
    product.stock = stock; /** remise du stock a jour */
    this.state.cartProduct.splice(this.state.cartProduct.indexOf(product), 1); /** supression du produit du panier */
    const newCart = this.state.cartProduct;
    const newPrice = this.state.totalPrice - ( product.price * inputValue ); /** gestion du prix */
    const newWeight = this.state.weight - (product.weight * inputValue); /**gestion du poids */
    this.setState( {cartProduct : newCart, totalPrice : newPrice, weight : newWeight} );  
  }

  filterChanged(newFilterText) {
    this.setState( {filterText : newFilterText} );
  }

  /**
   * augmente le stock du produit dans la boutique si sa quantité dimunie dans le panier
   * @param {*} productId id du produit
   * @param {*} inputValue value du input dans le panier
   * @returns value du input dans le panier (qu'on va reutiliser dans ProductCart)
   */
  increaseStock(productId, inputValue) {
    const product = this.state.cartProduct.find( product => product.id == productId);
    product.stock++;
    const newProducts = [...this.state.products];
    const newPrice = this.state.totalPrice - product.price; /** gestion du prix */
    const newWeight = this.state.weight - product.weight; /**gestion du poids */
    this.setState( {products : newProducts, totalPrice : newPrice, weight : newWeight} )
    return inputValue;
  }

  /**
   * augmente le stock du produit dans la boutique si sa quantité augmente dans le panier
   * @param {*} productId id du produit
   * @param {*} inputValue value du input dans le panier
   * @returns value du input dans le panier (qu'on va reutiliser dans ProductCart)
   */
  decreaseStock(productId, inputValue) {
    const product = this.state.cartProduct.find( product => product.id == productId);
    product.stock--;
    const newProducts = [...this.state.products];
    const newPrice = this.state.totalPrice + product.price; /** gestion du prix */
    const newWeight = this.state.weight + product.weight; /**gestion du poids */
    this.setState( {products : newProducts, totalPrice : newPrice, weight : newWeight} )
    return inputValue;
  }


  render() {
    const filter = this.filterChanged.bind(this);

    return (
      <div className='app'>
          < Boutique 
            products = {this.state.products}
            addToCart = {this.addToCart.bind(this)}
            filterChanged={filter}
            filterText={this.state.filterText}
          />
          <Panier 
            products = {this.state.cartProduct}
            addToCart={this.addToCart.bind(this)}
            deleteToCart ={this.deleteToCart.bind(this)}
            price={this.state.totalPrice}
            weight={this.state.weight}
            increaseStock={this.increaseStock.bind(this)}
            decreaseStock={this.decreaseStock.bind(this)}
          />
      </div>
    );
  }
}
